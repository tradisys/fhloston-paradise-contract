[maven]: https://maven.apache.org
[git]: https://git-scm.com
[audit]: https://audit.fomo.tradisys.com
[requirements]: https://gitlab.com/tradisys/fhloston-paradise-contract/wikis/Requirements-for-Transfers-Rewards


## Tradisys Fhloston Paradise 3 - Waves Transfer Reward

Requirements can be found on [wiki][requirements]

### Tests status
Latest tests report can be found at [tradisys tests results page][audit]

### How to build and run tests

To run the example you need to install [maven (3.1.1 or above)][maven] and [git][git].

To build and run Fhloston Paradise 3 tests follow instructions below.

#### Clone Repo
```bash
$ git clone https://gitlab.com/tradisys/fhloston-paradise-contract.git
```
#### Install tradisys dependencies into your local maven repository 
```bash
$ cd fhloston-paradise-contract
$ mvn install:install-file -Dfile=./lib/waves-itest-0.0.4.1.jar
$ mvn install:install-file -Dfile=./lib/tradisys-waves-toolbox-1.2.3-dev.jar
```

To verify that dependencies were correctly installed run build without tests
```bash
$ mvn clean package -DskipTests
```
#### Run Tests and review results

Modify `fhloston-paradise-contract/src/test/resources/itest.properties`:
* all attributes are mandatory here
* benz account which is specified through `itest.account.benz.private.key` property should contains 50 Waves (testnet)

Please be prepared that tests can takes about 30 min
```bash
$ mvn clean package
$ mvn allure:serve -Dallure.serve.port=63444 -Dallure.serve.host=127.0.0.1
```

[Atronax]: https://github.com/Atronax
#### Additional Thanks to
1. [Atronax][Atronax] for feedback about [fhloston3.rb](/fhloston3.ride), see 7dc6f744c706a8636c2e992cfb1d4cd13a726edd