package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.ConfigITest;
import com.tradisys.commons.waves.itest.Placeholder;
import com.tradisys.games.server.integration.TransactionsFactory;
import com.tradisys.games.server.utils.FormatUtils;
import com.wavesplatform.wavesj.Asset;
import com.wavesplatform.wavesj.PrivateKeyAccount;
import com.wavesplatform.wavesj.transactions.TransferTransaction;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.math.BigDecimal;

@Epic("Transfers and Burns Validation")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Fhloston3_TransferAndBurnTest extends AbstratQuickFomo3Test {

    private static final long TRANSFER_AMT = Asset.toWavelets(BigDecimal.ONE);

    @Override
    protected Fhloston3Ctx initCustomCtx() {
        Fhloston3Ctx ctx = super.initCustomCtx();
        ctx._maxRounds = new Placeholder<>("$maxRounds", 1);
        return ctx;
    }

    @Test
    @Description("Smart contract deployment")
    public void n005_deploy() throws IOException, InterruptedException {
        ctx()._blocksOnGameStart.setValue(getNode().getHeight() + 2);
        deployScript();
    }

    @Test(expected = IOException.class)
    @Description("Round 0 has NOT been started yet. DISALLOW Party owner (Admin) to post transfer tx (Waves) signed by own signature")
    public void n010_failToTransferByPartyOwnerBeforePartyStart() throws IOException, InterruptedException {
        transferFromSmart(ctx().partyOwnerAcc, TRANSFER_AMT);
    }

    @Test
    @Description("Round 0 has been started. Competition Stage. ALLOW Alice to invoke @move")
    public void n015_successMoves() throws IOException, InterruptedException {
        getNode().waitHeight(ctx()._blocksOnGameStart.getValue());
        invokeMove(ctx().aliceAcc);
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Competition Stage. DISALLOW Party owner (Admin) to post transfer tx (Waves) signed by own signature")
    public void n020_failToTransferByPartyOwnerAtTheCompetitionStage() throws Exception {
        transferFromSmart(ctx().partyOwnerAcc, TRANSFER_AMT);
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Competition Stage. DISALLOW anyone to post transfer tx (Waves) signed by own signature")
    public void n025_failToTransferByAnyAtTheCompetitionStage() throws Exception {
        transferFromSmart(ctx().caiAcc, TRANSFER_AMT);
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Winning height has been reached. Alice is winner. " +
            "DISALLOW Party owner (Admin) to post transfer tx from smart account.")
    public void n030_failToTransferByPartyOwnerAtThePauseStage() throws Exception {
        getNode().waitHeight(ctx().sharedState.currWinHeight);
        transferFromSmart(ctx().partyOwnerAcc, TRANSFER_AMT);
    }

    @Test
    @Description("Round 0. Alice is winner. ALLOW Alice to withdraw")
    public void n035_successWithdraw() throws Exception {
        withdrawAndAssertBalance(getBenzAcc(), ctx().winnersByRound.get(0), 0);
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Alice is winner. Withdraw tx posted. " +
            "DISALLOW Party owner (Admin) to post burn tx signed by own signature BEFORE party expiration period.")
    public void n040_failToBurnByPartyOwnerBeforePartyExpiration() throws Exception {
        getNode().waitHeight(getPartyFinishHeight() - 2);
        burn(ctx().partyOwnerAcc);
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Alice is winner. Withdraw tx posted." +
            "DISALLOW Party owner (Admin) to post transfer tx (Waves) signed by own signature")
    public void n045_failToTransferByPartyOwnerBeforePartyExpiration() throws Exception {
        transferFromSmart(ctx().partyOwnerAcc, TRANSFER_AMT);
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Alice is winner. Withdraw tx posted. Party expiration + withdrawPeriod has been reached. " +
            "DISALLOW anyone to post burn tx signed by own signature")
    public void n050_failToBurnByAnyAfterPartyExpiration() throws Exception {
        getNode().waitHeight(getPartyFinishHeight() + ctx()._withdrawPeriod.getValue() + 1);
        burn(ctx().aliceAcc);
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Alice is winner. Withdraw tx posted. Party expiration + withdrawPeriod has been reached. " +
            "DISALLOW anyone to post transfer tx (Waves) signed by own signature")
    public void n055_failToTransferByAnyAfterPartyExpiration() throws Exception {
        transferFromSmart(ctx().caiAcc, TRANSFER_AMT);
    }

    @Test
    @Description("Round 0. Alice is winner. Withdraw tx posted. Party expiration + withdrawPeriod has been reached. " +
            "ALLOW Party owner (Admin) to post burn tx signed by own signature")
    public void n060_successToBurnByPartyOwnerAfterPartyExpiration() throws Exception {
        burn(ctx().partyOwnerAcc);
    }

    @Test
    @Description("Round 0. Alice is winner. Withdraw tx posted. Party expiration + withdrawPeriod has been reached. " +
            "ALLOW Party owner (Admin) to post transfer tx (Waves) signed by own signature")
    public void n065_failToTransferByPartyOwnerAfterPartyExpiration() throws Exception {
        transferFromSmart(ctx().partyOwnerAcc, TRANSFER_AMT);
    }

    private void transferFromSmart(PrivateKeyAccount accBy, long amt) throws IOException, InterruptedException {
        TransferTransaction ttx = TransactionsFactory.makeTransferTx(ctx().smartAcc, accBy.getAddress(),
                amt, null,
                FormatUtils.toBlkMoney(ConfigITest.SCRIPT_TX_FEE), null,
                (String) null, TransactionsFactory.ProofsBuilder.signProof(accBy));
        String ttxId = getNode().send(ttx);
        getNode().waitTransaction(ttxId, getDefaultTimeout());
    }
}