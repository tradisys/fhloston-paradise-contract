package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.ConfigITest;
import com.tradisys.commons.waves.itest.Placeholder;
import com.wavesplatform.wavesj.PrivateKeyAccount;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;
import java.math.BigDecimal;

import static com.wavesplatform.wavesj.Asset.toWavelets;

@Epic("Fees Validation")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Fhloston3_Fees extends AbstratQuickFomo3Test {

    @Override
    protected Fhloston3Ctx initCustomCtx() {
        Fhloston3Ctx ctx = super.initCustomCtx();
        ctx._maxRounds = new Placeholder<>("$maxRounds", 2);
        return ctx;
    }

    @Test
    @Description("Smart contract deployment")
    public void n005_deploy() throws IOException, InterruptedException {
        deployScript();
    }

    /**
     * <b><i>Preconditions:</i></b>
     * <ul>
     *     <li>Round 0</li>
     *     <li>Competition Stage</li>
     * </ul>
     * <b><i>Hypothesis:</i></b>
     * <ul><li>
     * <font color="green"><b>ALLOW</b></font> invocation of <code>@move</code> with fee greater than min value (0.005 Waves).
     * </li></ul>
     * <b><i>Outcome state:</i></b>
     * <ul><li>Last move has been done by <i><b>Alice</b></i></li></ul>
     */
    @Test
    @Description("Round 0. Competition Stage. ALLOW invocations of @move with fee greater than min value (0.005 Waves)." +
            " Last move has been done by Alice.")
    public void n010_successMovesIfFeesGreaterThanMinInWaves() throws IOException, InterruptedException {
        customFeeInMove(getBenzAcc(), toWavelets(ConfigITest.SCRIPT_TX_FEE), null);
        customFeeInMove(ctx().aliceAcc, toWavelets(new BigDecimal("0.01")), null);
    }

    /**
     * <b><i>Preconditions:</i></b>
     * <ul>
     *     <li>Round 0</li>
     *     <li>Competition Stage</li>
     * </ul>
     * <b><i>Hypothesis:</i></b>
     * <ul><li>
     * <font color="red"><b>DISALLOW</b></font> Bob to invoke <code>@move</code> with fee less than min value (0.005 Waves).
     * </li></ul>
     * <b><i>Outcome state:</i></b>
     * <ul><li>Last move still has been done by <i><b>Alice</b></i></li></ul>
     */
    @Test(expected = IOException.class)
    @Description("Round0. Competition stage. DISALLOW Bob to invoke @move with fee less than min value (0.005 Waves). " +
            "Last move still has been done by Alice.")
    public void n015_failToMoveIfFeeLessThanMinInWaves() throws IOException, InterruptedException {
        customFeeInMove(ctx().bobAcc, toWavelets(ConfigITest.TRANSFER_FEE), null);
    }

    @Test(expected = IOException.class)
    @Description("Round0. Competition stage. DISALLOW Cai to invoke @move with fee greater than min value (0.005) but not in Waves. " +
            "Last move still has been done by Alice.")
    public void n015_failToMoveIfFeeGreaterThanMinButNotInWaves() throws IOException, InterruptedException {
        customFeeInMove(ctx().caiAcc, mrtFromDecimal(1), ctx()._mrtAssetId.getValue());
    }

    @Test(expected = IOException.class)
    @Description("Round0. Winning height has been reached. Winner is Alice. DISALLOW Alice to invoke @withdraw with fee less than min value (0.005 Waves)")
    public void n020_failToWithdrawIfInvFeeLessThanMinInWaves() throws IOException, InterruptedException {
        getNode().waitHeight(ctx().sharedState.currWinHeight);

        customFeeInWithdraw(getBenzAcc(), ctx().aliceAcc, 0, MIN_FEE_AMT - 1, null);
    }

    @Test(expected = IOException.class)
    @Description("Round0. Winning height has been reached. Winner is Alice. DISALLOW Alice to invoke @withdraw with correct fee value (0.005) but not in Waves")
    public void n025_failToWithdrawIfInvFeeCorrectButNotInWaves() throws IOException, InterruptedException {
        getNode().waitHeight(ctx().sharedState.currWinHeight);
        customFeeInWithdraw(getBenzAcc(), ctx().aliceAcc, 0, MIN_FEE_AMT, ctx()._mrtAssetId.getValue());
    }

    @Test
    @Description("Round0. Winning height has been reached. Winner is Alice. ALLOW Alice to invoke @withdraw with correct fee value (0.005) in Waves")
    public void n030_successToWithdrawIfInvFeeCorrectInWaves() throws IOException, InterruptedException {
        getNode().waitHeight(ctx().sharedState.currWinHeight);
        customFeeInWithdraw(getBenzAcc(), ctx().aliceAcc, 0, MIN_FEE_AMT, null);
    }

    @Test
    @Description("Round 1 has been started. ALLOW to invoke @move with correct fee. " +
            "Last move has been done by Bob.")
    public void n040_successMovesInRound2() throws IOException, InterruptedException {
        getNode().waitHeight(getRoundStartingHeight(1));
        invokeMove(ctx().aliceAcc);
        invokeMove(ctx().bobAcc);
    }

    @Test
    @Description("Round 1 and MRT party and withdraw period finished. " +
            "Winner of Round 1 is Bob. Bob didn't withdraw his prize. " +
            "ALLOW Anyone to invoke @withdraw and validate that prize will be transferred to partyOwner address")
    public void n045_successWithdrawToPartyOwnerAfterPartyFinishPlusWithdrawPeriod() throws IOException, InterruptedException {
        getNode().waitHeight(getPartyFinishHeight() + ctx()._withdrawPeriod.getValue() + 1);
        withdrawAndAssertBalance(getBenzAcc(), ctx().partyOwnerAcc, 1);
    }

    @Test(expected = IOException.class)
    @Description("Round 1 and MRT party and withdraw period finished. " +
            "DISALLOW to burn mrt by party owner with fee less than min value (0.005 Waves)")
    public void n050_failToBurnMrtByPartyOwnerWithFeeLessThanFixedFee() throws Exception {
        burn(ctx().partyOwnerAcc, MIN_FEE_AMT - 1);
    }

    @Test(expected = IOException.class)
    @Description("Round 1 and MRT party and withdraw period finished. " +
            "DISALLOW to burn mrt by party owner with fee greater than min value (0.005 Waves)")
    public void n055_failToBurnMrtByPartyOwnerWithFeeGreaterThanFixedFee() throws Exception {
        burn(ctx().partyOwnerAcc, MIN_FEE_AMT + 1);
    }

    @Test(expected = IOException.class)
    @Description("Round 1 and MRT party and withdraw period finished. " +
            "DISALLOW to burn mrt by anyone except party owner with correct fee (0.005 Waves)")
    public void n060_failToBurnMrtByAnyExceptPartyOwnerWithCorrectFee() throws Exception {
        burn(ctx().aliceAcc, MIN_FEE_AMT);
    }

    @Test
    @Description("Round 1 and MRT party and withdraw period finished. " +
            "ALLOW to burn mrt by party owner with correct fee (0.005 Waves)")
    public void n065_successToBurnMrtByPartyOwnerWithCorrectFee() throws Exception {
        burn(ctx().partyOwnerAcc, MIN_FEE_AMT);
    }

    private void customFeeInMove(PrivateKeyAccount acc, long fee, String feeAssetId) throws IOException, InterruptedException {
        invokeMove(acc, ctx().smartAcc.getAddress(), ctx()._pmtStep.getValue(), ctx()._mrtAssetId.getValue(), fee, feeAssetId);
    }

    private void customFeeInWithdraw(PrivateKeyAccount initiator, PrivateKeyAccount winner, int round, long invFee, String invFeeAssetId) throws IOException, InterruptedException {
        withdrawAndAssertBalance(initiator, winner, round, invFee, invFeeAssetId);
    }
}