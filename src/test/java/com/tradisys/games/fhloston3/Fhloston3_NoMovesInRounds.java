package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.Placeholder;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

@Epic("No moves in round validation")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Fhloston3_NoMovesInRounds extends AbstratQuickFomo3Test {

    @Override
    protected Fhloston3Ctx initCustomCtx() {
        Fhloston3Ctx ctx = super.initCustomCtx();
        ctx._maxRounds = new Placeholder<>("$maxRounds", 3);
        return ctx;
    }

    @Test
    @Description("Smart contract deployment")
    public void n005_deploy() throws IOException, InterruptedException {
        deployScript();
    }

    @Test(expected = IOException.class)
    @Description("Round 0 had been started. Nobody had moved. The first countdown has finished. " +
            "DISALLOW Alice to invoke @move")
    public void n010_failToMoveR0WithoutMovesAfterTheCountdown() throws IOException, InterruptedException {
        getNode().waitHeight(ctx()._blocksOnGameStart.getValue() + ctx()._heightStep.getValue());
        invokeMove(ctx().aliceAcc);
    }

    @Test(expected = IOException.class)
    @Description("Round 0 has no winner." +
            "DISALLOW Party owner (Admin) to withdraw")
    public void n015_failToWithdrawR0WithoutWinnerByPartyOwner() throws IOException, InterruptedException {
        withdraw(getBenzAcc(), 0);
    }

    @Test(expected = IOException.class)
    @Description("Round 0 has no winner. " +
            "DISALLOW anyone to withdraw")
    public void n020_failToWithdrawR0WithoutWinnerByAny() throws IOException, InterruptedException {
        withdraw(ctx().aliceAcc, 0);
    }

    @Test(expected = IOException.class)
    @Description("Round 1 has been started. Round 0 has no winner. " +
            "DISALLOW anyone to withdraw for Round 0")
    public void n025_failToWithdrawR0ByAnyIfRound1InProgress() throws IOException, InterruptedException {
        getNode().waitHeight(getRoundStartingHeight(1) + 1);
        withdraw(ctx().aliceAcc, 0);
    }

    @Test
    @Description("Round 1 at competition stage. ALLOW Alice to invoke @move. Last move has been done by Alice.")
    public void n030_successMoveR1ByAlice() throws Exception {
        invokeMove(ctx().aliceAcc);
    }

    @Test(expected = IOException.class)
    @Description("Round 1 at competition stage. DISALLOW Alice (round 1 leader) to withdraw for Round 0")
    public void n035_failToWithdrawR0ByR1CurrentLeader() throws Exception {
        withdraw(ctx().aliceAcc, 0);
    }

    @Test(expected = IOException.class)
    @Description("Round 1. Winning height has been reached. Alice is winner. " +
            "DISALLOW Alice (round 1 winner) to withdraw for Round 0")
    public void n040_failToWithdrawR0ByR1Winner() throws Exception {
        getNode().waitHeight(ctx().sharedState.currWinHeight);
        withdraw(ctx().aliceAcc, 0);
    }

    @Test(expected = IOException.class)
    @Description("Round 2 had been started. Nobody had moved. The countdown has finished. " +
            "DISALLOW Alice to invoke @move")
    public void n045_failToMoveR2WithoutMovesAfterTheCountdown() throws Exception {
        getNode().waitHeight(getRoundStartingHeight(2) + ctx()._heightStep.getValue());
        invokeMove(ctx().aliceAcc);
    }

    @Test(expected = IOException.class)
    @Description("Round 2 finished. Round 2 has no winner. " +
            "DISALLOW Alice (winner of round 1) to withdraw for round 2")
    public void n050_failToWithdrawR2WithoutWinnerByR1Winner() throws Exception {
        withdraw(ctx().aliceAcc, 2);
    }

    @Test
    @Description("Alice is a winner of Round 1. Nobody has win in Round 2 and Round 0. " +
            "ALLOW Anyone to withdraw for Round 1")
    public void n055_successWithdrawR1ByR1Winner() throws Exception {
        withdrawAndAssertBalance(getBenzAcc(), ctx().aliceAcc, 1);
    }

    @Test(expected = IOException.class)
    @Description("Alice is a winner of Round 1. Alice has done withdraw for Round 1. Nobody has win in Round 2 and Round 0. " +
            "DISALLOW Alice to withdraw for round 0")
    public void n060_failToWithdrawR0ByR1WinnerAfterR1SuccessWithdraw() throws Exception {
        withdraw(ctx().aliceAcc, 0);
    }

    @Test(expected = IOException.class)
    @Description("Alice is a winner of Round 1. Alice has done withdraw for Round 1. Nobody has win in Round 2 and Round 0. " +
            "DISALLOW Alice to withdraw for round 2")
    public void n065_failToWithdrawR2ByR1WinnerAfterR1SuccessWithdraw() throws Exception {
        withdraw(ctx().aliceAcc, 0);
    }
}