package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.Placeholder;
import com.wavesplatform.wavesj.Account;
import com.wavesplatform.wavesj.PublicKeyAccount;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class ProductionDeploy extends AbstractFomo3Test {

    @Override
    protected Fhloston3Ctx initCustomCtx() {
        Fhloston3Ctx ctx = new Fhloston3Ctx();

        ctx.prod = true;

        int currHeight = getNode().getHeightSafe();
        ctx._maxRounds                  = new Placeholder<>("$maxRounds",            60);
        ctx._pmtStep                    = new Placeholder<>("$pmtStep",              100);
        ctx._partyOwnerPubKey           = new Placeholder<>("$partyOwnerPubKey",     "5iDk2vYUH5w9nvKjSqvnXdHGsYQW4MahQ5EuUknXZLMA");
        ctx._withdrawPeriod             = new Placeholder<>("$withdrawPeriod",       43200);
        ctx._winAmt                     = new Placeholder<>("$winAmt",               500);
        ctx._heightStep                 = new Placeholder<>("$heightStep",           15);
        ctx._blocksPerRound             = new Placeholder<>("$blocksPerRound",       1460);
        ctx._blocksPerCompetition       = new Placeholder<>("$blocksPerCompetition", 1450);
        ctx._mrtAssetId                 = new Placeholder<>("$mrtAssetId",           "4uK8i4ThRGbehENwa6MxyLtxAjAo1Rj9fduborGExarC");
        ctx._blocksOnGameStart          = new Placeholder<>("$blocksOnGameStart",    1824280);

        return ctx;
    }

    @Test
    public void n000_deploy() throws Exception {
        deployScript();
    }

    @Test
    public void n005_validatePublicKey() {
        PublicKeyAccount pub = new PublicKeyAccount(ctx()._partyOwnerPubKey.getValue(), Account.MAINNET);
        Assert.assertEquals("3PETNFhSiTMopMyM9fvXpKbApcPZec2jzzj", pub.getAddress());
    }
}
