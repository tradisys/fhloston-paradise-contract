package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.BaseJUnitITest;
import com.tradisys.commons.waves.itest.ConfigITest;
import com.tradisys.commons.waves.itest.Placeholder;
import com.tradisys.games.server.exception.BlkChTimeoutException;
import com.tradisys.games.server.integration.AssetBalanceInfo;
import com.tradisys.games.server.integration.BalanceInfo;
import com.tradisys.games.server.integration.Fees;
import com.tradisys.games.server.utils.DefaultPredicates;
import com.wavesplatform.wavesj.*;
import com.wavesplatform.wavesj.transactions.*;
import org.junit.Before;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.tradisys.games.server.utils.FormatUtils.toServerMoney;
import static com.wavesplatform.wavesj.Asset.toWavelets;

public abstract class AbstractFomo3Test extends BaseJUnitITest<Fhloston3Ctx> {

    static final long MIN_FEE_AMT = toWavelets(ConfigITest.SCRIPT_TX_FEE);
    static final String MRT_ASSET_ID = "4uK8i4ThRGbehENwa6MxyLtxAjAo1Rj9fduborGExarC";
    static final String SHARED_STATE_KEY = "RoundsSharedState";

    public AbstractFomo3Test() {
        super(Fhloston3Ctx.class);
    }

    @Override
    protected Fhloston3Ctx initCustomCtx() {
        Fhloston3Ctx ctx = new Fhloston3Ctx();

        ctx.partyOwnerAcc = generateNewAccount("Party Owner", true);

        ctx._maxRounds                  = new Placeholder<>("$maxRounds",               100*1000*1000);
        ctx._pmtStep                    = new Placeholder<>("$pmtStep",                 10);
        ctx._partyOwnerPubKey           = new Placeholder<>("$partyOwnerPubKey",
                                                            Base58.encode(ctx.partyOwnerAcc.getPublicKey()));
        ctx._withdrawPeriod             = new Placeholder<>("$withdrawPeriod",          10);
        ctx._winAmt                     = new Placeholder<>("$winAmt",                  1);
        ctx._heightStep                 = new Placeholder<>("$heightStep",              2);
        ctx._blocksPerRound             = new Placeholder<>("$blocksPerRound",          12);
        ctx._blocksPerCompetition       = new Placeholder<>("$blocksPerCompetition",    8);
        ctx._mrtAssetId                 = new Placeholder<>("$mrtAssetId",              System.getProperty("mrtAssetId"));
        ctx._blocksOnGameStart          = new Placeholder<>("$blocksOnGameStart",       null);

        // Alice Account generated: address=3NBQxCjpxwncy2Qfcjwyu5VNs7fzSVPmCNx
        // public=EJ1xDTwRjiLJ21Sw8fHt2mcs9oE1BxrtSmanmRmFjkoK private=7Tz1jMVSxyAnNjYCNnF1TeDJb9bBjR9nJ139BWnuN4E9
        // seed=protect ride citizen feed code square rare floor stumble wisdom school guilt health raw pitch
        ctx.aliceAcc = null; //PrivateKeyAccount.fromPrivateKey("7Tz1jMVSxyAnNjYCNnF1TeDJb9bBjR9nJ139BWnuN4E9", getChainId());

        // Bob Account generated: address=3MsK86HVEbVT8JQKvE63Z9UcQ9J5JwEAUbs
        // public=BAS84NxBvCAF1wkatS3XCZPSWQt3WGdzUPTiorjfZ4bV private=CrZ5CyGqewjwUJhQjM7x7n1b5n9sS4FBoNULcDGXkzgN
        // seed=wink believe ethics arctic infant umbrella push spice whale door jaguar mule vacuum fly egg
        ctx.bobAcc = null;//PrivateKeyAccount.fromPrivateKey("CrZ5CyGqewjwUJhQjM7x7n1b5n9sS4FBoNULcDGXkzgN", getChainId());

        // Cai Account generated: address=3MpkJu7L27y4vWcGZdAVQ9AMeZiVwyJb7g7
        // public=Fhfz8jJoxctuwke3LyJHsQ66zRaBkAxosfBrEYUAtiDf private=DSHCoTw2JPuBsjRqZ9VukDfcpeKw9GdEfQrhqHjWuXHy
        // seed=elegant expect front extend joy buyer valve surface company appear funny rubber deputy arrive provide
        ctx.caiAcc = null;//PrivateKeyAccount.fromPrivateKey("DSHCoTw2JPuBsjRqZ9VukDfcpeKw9GdEfQrhqHjWuXHy", getChainId());

        return ctx;
    }

    @Before
    public void beforeTest() throws IOException, InterruptedException {
        if (ctx()._mrtAssetId.getValue() != null && !ctx().prod) {
            refillAllAccounts();
        }
    }

    protected synchronized void refillAllAccounts() throws IOException, InterruptedException {
        ctx().aliceAcc = refill(ctx().aliceAcc, "Alice Acc");
        ctx().bobAcc = refill(ctx().bobAcc, "Bob Acc");
        ctx().caiAcc = refill(ctx().caiAcc, "Cai Acc");
        ctx().partyOwnerAcc = refill(ctx().partyOwnerAcc, "Party Owner");

        if (ctx().smartAcc != null) {
            String smartAddress = ctx().smartAcc.getAddress();
            long smartBalance = getNode().getBalanceInfo(smartAddress).getAvailable();
            long minLeaseAmt = toWavelets(ctx()._winAmt.getValue());

            if (smartBalance < minLeaseAmt) {
                String txId = getNode().getNode().transfer(getBenzAcc(), smartAddress, minLeaseAmt * 2,
                        toWavelets(ConfigITest.TRANSFER_FEE), "");
                getNode().waitTransaction(txId, getDefaultTimeout());
            }
        }
    }

    protected synchronized PrivateKeyAccount refill(PrivateKeyAccount acc, String name) throws IOException, InterruptedException {
        long wavesAmt = Asset.toWavelets(BigDecimal.ONE);
        long wavesBalance = 0;

        boolean refillMrt = true;
        boolean refillWaves = true;
        if (acc != null) {
            wavesBalance = getNode().getBalanceInfo(acc.getAddress()).getAvailable();

            refillMrt = getNode().getNode().getBalance(acc.getAddress(), ctx()._mrtAssetId.getValue()) < mrtFromDecimal(100);
            refillWaves = wavesBalance < toWavelets(new BigDecimal("0.005"));
        } else {
            acc = generateNewAccount(name, true);
        }

        String mrtId = null;
        if (refillMrt) {
            mrtId = getNode().getNode().transfer(getBenzAcc(), acc.getAddress(), mrtFromDecimal(1000), ctx()._mrtAssetId.getValue(),
                    toWavelets(ConfigITest.TRANSFER_FEE), null, "");
        }

        String wavesId = null;
        if (refillWaves) {
            wavesId = getNode().getNode().transfer(getBenzAcc(), acc.getAddress(), wavesAmt,
                    null, toWavelets(ConfigITest.TRANSFER_FEE), null, "");
        }

        if (mrtId != null) {
            getNode().waitTransaction(mrtId, getDefaultTimeout());
        }

        if (wavesId != null) {
            getNode().waitTransaction(wavesId, getDefaultTimeout());
            assertBalance(wavesBalance + wavesAmt, acc.getAddress());
        }

        return acc;
    }

    protected void deployScript() throws IOException, InterruptedException {
        if (ctx().smartAcc == null) {
            ctx().smartAcc = generateNewAccount("Smart Acc", true, BigDecimal.ONE.multiply(BigDecimal.valueOf(2)));

            long mrtBalance = 0;
            if (ctx()._mrtAssetId.getValue() != null) {
                mrtBalance = getNode().getAssetBalance(getBenzAcc().getAddress(), ctx()._mrtAssetId.getValue()).getBalance();
            }

            if (!ctx().prod && mrtBalance < mrtFromDecimal(1000)) {
                getLogger().warn("Not enough MRT on balance={}. New asset will be generated for tests", mrtBalance);
                String assetId = issueAsset(getBenzAcc(), mrtFromDecimal(100*1000*1000L), (byte)2, "Fomo3", false);
                ctx()._mrtAssetId.setValue(assetId);
            }

            if (ctx().prod && MRT_ASSET_ID.compareTo(ctx()._mrtAssetId.getValue()) != 0) {
                String msg = String.format("Invalid MRT asset id is configured for production: required=%1$s actual=%2$s",
                        MRT_ASSET_ID, ctx()._mrtAssetId.getValue());
                getLogger().error(msg);
                throw new IllegalStateException(msg);
            }

            PrivateKeyAccount smartAcc = ctx().smartAcc;

            if (ctx()._blocksOnGameStart.getValue() == null) {
                if (ctx().prod) {
                    String msg = "Please config explicit _blocksOnGameStart parameter for production deployment";
                    getLogger().error(msg);
                    throw new IllegalStateException(msg);
                }
                ctx()._blocksOnGameStart.setValue(getNode().getHeight());
            }

            Placeholder<?>[] placeholders = ctx().getPlaceholders();
            List<DataEntry<?>> dataEntries = Arrays.stream(placeholders)
                    .map(p -> new DataEntry.StringEntry(p.getKey(), p.getValue().toString()))
                    .collect(Collectors.toList());
            dataEntries.add(new DataEntry.StringEntry("RoundsSharedState", "?_0_?"));
            DataTransaction dataTx = Transactions.makeDataTx(smartAcc, dataEntries, toWavelets(ConfigITest.SCRIPT_TX_FEE));

            long balance = getNode().getBalanceInfo(smartAcc.getAddress()).getAvailable();
            String txId = getNode().send(dataTx);
            getLogger().info("Data transaction id: {}", txId);
            getNode().waitMoneyOnBalance(smartAcc.getAddress(), toServerMoney(balance), ConfigITest.SCRIPT_TX_FEE.negate(),
                    DefaultPredicates.EQUALS, getDefaultTimeout());

            String script = readScript("fhloston3.ride", ctx().getPlaceholders());
            deployScript(smartAcc, script);
        }
        beforeTest();
    }

    protected int getRoundNum(int height) {
        int offset = height - ctx()._blocksOnGameStart.getValue();
        return offset / ctx()._blocksPerRound.getValue();
    }

    protected int getPartyFinishHeight() {
        return ctx()._blocksOnGameStart.getValue()
                + ctx()._blocksPerRound.getValue() * ctx()._maxRounds.getValue()
                + ctx()._withdrawPeriod.getValue();
    }

    protected int getHeightWhenFullStepIsUsed(int round) {
        return getRoundCompetitionEndHeight(round) - ctx()._heightStep.getValue();
    }

    protected int getRoundCompetitionEndHeight(int round) {
        return getRoundStartingHeight(round) + ctx()._blocksPerCompetition.getValue();
    }

    protected int getRoundStartingHeight(int round) {
        return ctx()._blocksOnGameStart.getValue() + round * ctx()._blocksPerRound.getValue();
    }

    protected long getValidWinAmt() {
        return toWavelets(BigDecimal.valueOf(ctx()._winAmt.getValue()));
    }

    protected String invokeMove(PrivateKeyAccount acc)
            throws IOException, InterruptedException {
        return invokeMove(acc, ctx()._pmtStep.getValue(), ctx()._mrtAssetId.getValue());
    }

    protected String invokeMove(PrivateKeyAccount acc, int amt, String assetId)
            throws IOException, InterruptedException {
        return invokeMove(acc, ctx().smartAcc.getAddress(), amt, assetId);
    }

    protected String invokeMove(PrivateKeyAccount acc, String fomoDapp, int amt, String assetId)
            throws IOException, InterruptedException {
        return invokeMove(acc, fomoDapp, amt, assetId, toWavelets(ConfigITest.SCRIPT_TX_FEE), null);
    }

    protected String invokeMove(PrivateKeyAccount acc, String fomoDapp, int amt, String assetId, long fee, String feeAssetId)
            throws IOException, InterruptedException {

        getLogger().info("MOVE by {}", acc.getAddress());
        InvokeScriptTransaction inv = new InvokeScriptTransaction(getChainId(), acc, fomoDapp,
                "move", fee, feeAssetId, System.currentTimeMillis())
                .withPayment(mrtFromDecimal(amt), assetId)
                .sign(acc);
        String txId = getNode().send(inv);
        Transaction txInfo = getNode().waitTransaction(txId, getDefaultTimeout());
        ctx().sharedState = waitPlayerInSharedState(acc, getDefaultTimeout());
        ctx().currWinAcc = acc;
        ctx().winnersByRound.put(getRoundNum(txInfo.getHeight()), acc);
        return txId;
    }

    protected void withdrawAndAssertBalance(PrivateKeyAccount withdrawInitiator, PublicKeyAccount winner, int round)
            throws IOException, InterruptedException {
        withdrawAndAssertBalance(withdrawInitiator, winner, round, Fees.WAVES.SCRIPT_TX_FEE, null);
    }

    protected void withdrawAndAssertBalance(PrivateKeyAccount withdrawInitiator, PublicKeyAccount winner, int round,
                                            long invFee, String invFeeAssetId)
            throws IOException, InterruptedException {
        long winnerBalanceBefore = getNode().getBalanceInfo(winner.getAddress()).getAvailable();
        long winAmt = Asset.toWavelets(ctx()._winAmt.getValue());
        withdraw(withdrawInitiator, round, invFee, invFeeAssetId);
        assertBalance(winnerBalanceBefore + winAmt, winner.getAddress());
    }

    protected String withdraw(PrivateKeyAccount acc, int round) throws IOException, InterruptedException {
        return withdraw(acc, round, toWavelets(ConfigITest.SCRIPT_TX_FEE), null);
    }

    protected String withdraw(PrivateKeyAccount acc, int round, long invFee, String invFeeAssetId) throws IOException, InterruptedException {
        getLogger().info("SEND WITHDRAW by {}", acc.getAddress());

        InvokeScriptTransaction inv = new InvokeScriptTransaction(getChainId(), acc, ctx().smartAcc.getAddress(),
                "withdraw", invFee, invFeeAssetId, System.currentTimeMillis())
                .withArg(round)
                .sign(acc);

        String invId = getNode().send(inv);
        getNode().waitTransaction(invId, getDefaultTimeout());
        waitWithdrawIdInRoundState(round, invId, getDefaultTimeout());

        getLogger().info("WITHDRAW SUCCESS: txId={}", invId);

        return invId;
    }

    protected String burn(PrivateKeyAccount acc) throws IOException, InterruptedException {
        return burn(acc, Fees.WAVES.SCRIPT_TX_FEE);
    }

    protected String burn(PrivateKeyAccount acc, long fee) throws IOException, InterruptedException {
        String assetId = ctx()._mrtAssetId.getValue();
        long balance = getNode().getAssetBalance(ctx().smartAcc.getAddress(), assetId).getBalance();

        BurnTransactionV2 burnTx = new BurnTransactionV2(ctx().smartAcc, getChainId(),
                assetId, balance, fee, System.currentTimeMillis());

        ByteString sig = new ByteString(acc.sign(burnTx));

        burnTx = new BurnTransactionV2(burnTx.getSenderPublicKey(), burnTx.getChainId(), burnTx.getAssetId(),
                burnTx.getAmount(), burnTx.getFee(), burnTx.getTimestamp(), Collections.singletonList(sig));

        String txId = getNode().send(burnTx);
        getNode().waitTransaction(txId, getDefaultTimeout());

        whileInState(() -> getAssetBalanceSilently(ctx().smartAcc.getAddress(), assetId),
                b -> b.getBalance() != 0, getDefaultTimeout());

        return txId;
    }

    protected SharedState waitPlayerInSharedState(PublicKeyAccount acc, long timeout) throws InterruptedException, IOException {
        String player = acc.getAddress();
        return whileInState(this::getSharedState,
                s -> s.lastParticipantAddress.compareTo(player) != 0,
                timeout);
    }

    protected RoundState waitWithdrawIdInRoundState(final int round, String leaseId, long timeout) throws InterruptedException, IOException {
        return whileInState(() -> getRoundInfo(round),
                r -> leaseId.compareTo(r.withdrawId) != 0,
                timeout);
    }

    protected <T> T whileInState(Supplier<Optional<T>> stateSupplier, Predicate<T> loopPredicate, long timeout) throws InterruptedException, IOException {
        Optional<T> state = Optional.empty();
        long start = System.currentTimeMillis();

        while (!state.isPresent()
                    || loopPredicate.test(state.get())) {
            try {
                state = stateSupplier.get();
            } catch (Exception ex) {
                getLogger().warn("Error during whileInState", ex);
            }
            Thread.sleep(1000);

            if (System.currentTimeMillis() - start >= timeout) {
                getLogger().error("Waiting state was stopped by timeout");
                throw new BlkChTimeoutException("Waiting state was stopped by timeout");
            }
        }

        return state.get();
    }

    protected Optional<SharedState> getSharedState() {
        try {
            return getNode().getAccountDataByKey(ctx().smartAcc.getAddress(), SHARED_STATE_KEY)
                    .map(dataEntry -> {
                        String data = dataEntry.getValue();
                        String[] tokens = data.split("_");
                        int i = 0;
                        String roundStr = tokens[i++];
                        String gameNumStr = tokens[i++];
                        String lastParticipants = tokens.length > 2 ? tokens[i++] : "";

                        return new SharedState(
                                roundStr.compareTo("?") == 0 ? -1 : Integer.parseInt(roundStr),
                                Integer.parseInt(gameNumStr),
                                lastParticipants
                        );
                    });
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected Optional<RoundState> getRoundInfo(int round) {
        try {
            return getNode().getAccountDataByKey(ctx().smartAcc.getAddress(), "round" + round)
                    .map(dataEntry -> {
                        String data = dataEntry.getValue();
                        String[] tokens = data.split("_");
                        int i = 0;
                        return new RoundState(
                                Integer.parseInt(tokens[i++]),
                                tokens[i++],
                                tokens[i++],
                                tokens[i++]
                        );
                    });
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void assertBalance(long expected, String address) throws InterruptedException, IOException {
        getLogger().info("Assert balance: address={} expectedAmt={}", address, expected);
        whileInState(() -> getBalanceSilently(address), b -> b.getAvailable() != expected, getDefaultTimeout());
    }

    public Optional<BalanceInfo> getBalanceSilently(String address) {
        BalanceInfo balanceInfo = null;
        try {
            balanceInfo = getNode().getBalanceInfo(address);
        } catch (Throwable ex) {
            // do nothing
        }
        return Optional.ofNullable(balanceInfo);
    }

    public Optional<AssetBalanceInfo> getAssetBalanceSilently(String address, String assetId) {
        AssetBalanceInfo balanceInfo = null;
        try {
            balanceInfo = getNode().getAssetBalance(address, assetId);
        } catch (Throwable ex) {
            // do nothing
        }
        return Optional.ofNullable(balanceInfo);
    }

    protected PrivateKeyAccount getLooserAcc() {
        if (ctx().aliceAcc != ctx().currWinAcc) {
            return ctx().aliceAcc;
        } else {
            return ctx().bobAcc;
        }
    }

    protected static long mrtFromDecimal(long mrtAmt) {
        return mrtAmt * 100;
    }

    protected static class RoundState {
        int currWinHeight;
        String currWinAddress;
        String currWinnerPubKey;
        String withdrawId;

        public RoundState(int currWinHeight, String currWinAddress, String currWinnerPubKey, String withdrawId) {
            this.currWinHeight = currWinHeight;
            this.currWinAddress = currWinAddress;
            this.currWinnerPubKey = currWinnerPubKey;
            this.withdrawId = withdrawId;
        }
    }

    protected static class SharedState {
        int currWinHeight;
        int gameNum;
        String lastParticipantAddress;

        public SharedState(int currWinHeight, int gameNum, String lastParticipants) {
            this.currWinHeight = currWinHeight;
            this.gameNum = gameNum;
            this.lastParticipantAddress = lastParticipants.isEmpty() ? "" : lastParticipants.substring(1).split("-")[0];
        }
    }
}