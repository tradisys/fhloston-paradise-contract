package com.tradisys.games.fhloston3;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

@Epic("Simple scenario to quickly validate minimal rules")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Fhloston3_R0SimplePositiveScenario extends AbstractFomo3Test {

    @Test
    @Description("Smart contract deployment")
    public void n005_deploy() throws IOException, InterruptedException {
        deployScript();
    }

    @Test(expected = IOException.class)
    @Description("Impossible to play (make a move) with attached payment in Waves")
    public void n010_failToMoveWithPaymentInWaves() throws IOException, InterruptedException {
        getNode().waitHeight(ctx()._blocksOnGameStart.getValue(), getDefaultTimeout());
        invokeMove(getBenzAcc(), ctx()._pmtStep.getValue(), null);
    }

    @Test
    @Description("Several players are playing sequentially. Cai is the last player.")
    public void n015_successMovesByPlayers() throws IOException, InterruptedException {
        invokeMove(ctx().aliceAcc);
        invokeMove(ctx().bobAcc);
        invokeMove(ctx().caiAcc);
    }

    @Test(expected = IOException.class)
    @Description("At the prev. step Cai has been the last player. We are waiting winning height and validating that " +
        " impossible to move by other players at the win height.")
    public void n020_failToMoveAfterWinInTheMiddleOfCompetition() throws IOException, InterruptedException {
        int currWinHeight = ctx().sharedState.currWinHeight;
        getLogger().info("Waiting WIN height {}", currWinHeight);
        getNode().waitHeight(currWinHeight);
        invokeMove(getBenzAcc(), ctx()._pmtStep.getValue(), ctx()._mrtAssetId.getValue());
    }

    @Test
    @Description("Cai is winner. We are validating that anyone can post withdraw and Cai receives prize")
    public void n025_successWithdrawByLooser() throws Exception {
        withdrawAndAssertBalance(getLooserAcc(), ctx().caiAcc, 0);
    }
}
