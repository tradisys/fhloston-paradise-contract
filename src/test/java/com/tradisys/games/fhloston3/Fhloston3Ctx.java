package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.BaseJUnitITest;
import com.tradisys.commons.waves.itest.Placeholder;
import com.wavesplatform.wavesj.PrivateKeyAccount;

import java.util.HashMap;
import java.util.Map;

public class Fhloston3Ctx implements BaseJUnitITest.CustomCtx {

    Placeholder<Integer> _maxRounds;
    Placeholder<Integer> _pmtStep;
    Placeholder<String>  _partyOwnerPubKey;
    Placeholder<Integer> _withdrawPeriod;
    Placeholder<Integer> _winAmt;
    Placeholder<Integer> _heightStep;
    Placeholder<Integer> _blocksPerRound;
    Placeholder<Integer> _blocksPerCompetition;
    Placeholder<String>  _mrtAssetId;
    Placeholder<Integer> _blocksOnGameStart;

    PrivateKeyAccount smartAcc;
    PrivateKeyAccount aliceAcc;
    PrivateKeyAccount bobAcc;
    PrivateKeyAccount caiAcc;
    PrivateKeyAccount partyOwnerAcc;

    PrivateKeyAccount currWinAcc = null;
    AbstractFomo3Test.SharedState sharedState = null;
    Map<Integer, PrivateKeyAccount> winnersByRound = new HashMap<>();
    boolean prod = false;

    public Placeholder<?>[] getPlaceholders() {
        return new Placeholder<?>[] {
                _maxRounds, _pmtStep, _partyOwnerPubKey,
                _withdrawPeriod,
                _winAmt, _heightStep,
                _blocksOnGameStart, _blocksPerRound, _blocksPerCompetition, _mrtAssetId
        };
    }
}