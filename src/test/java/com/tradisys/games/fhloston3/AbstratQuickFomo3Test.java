package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.Placeholder;

public abstract class AbstratQuickFomo3Test extends AbstractFomo3Test {

    @Override
    protected Fhloston3Ctx initCustomCtx() {
        Fhloston3Ctx ctx = super.initCustomCtx();

        ctx._maxRounds                  = new Placeholder<>("$maxRounds",               1000*1000*1000);
        ctx._withdrawPeriod             = new Placeholder<>("$withdrawPeriod",          5);
        ctx._winAmt                     = new Placeholder<>("$winAmt",                  1);
        ctx._heightStep                 = new Placeholder<>("$heightStep",              3);
        ctx._blocksPerRound             = new Placeholder<>("$blocksPerRound",          7);
        ctx._blocksPerCompetition       = new Placeholder<>("$blocksPerCompetition",    4);

        return ctx;
    }
}