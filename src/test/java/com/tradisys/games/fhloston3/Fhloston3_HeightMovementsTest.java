package com.tradisys.games.fhloston3;

import com.tradisys.commons.waves.itest.ConfigITest;
import com.tradisys.commons.waves.itest.Placeholder;
import com.wavesplatform.wavesj.PrivateKeyAccount;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.io.IOException;

@Epic("Moves Validation")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Fhloston3_HeightMovementsTest extends AbstratQuickFomo3Test {

    @Override
    protected Fhloston3Ctx initCustomCtx() {
        Fhloston3Ctx ctx = super.initCustomCtx();
        ctx._maxRounds              = new Placeholder<>("$maxRounds", 2);
        return ctx;
    }

    @Test
    @Description("Smart contract deployment")
    public void n005_deploy() throws Exception {
        ctx()._blocksOnGameStart.setValue(getNode().getHeight() + 3);
        deployScript();
    }

    @Test(expected = IOException.class)
    @Description("Round 0 has not been started yet. DISALLOW Alice to invoke @move.")
    public void n010_failToMoveBeforeRoundStart() throws Exception {
        invokeMove(ctx().aliceAcc);
    }

    @Test
    @Description("Round 0 has been started. Alice, Bob and Cai plays till the last round's height.")
    public void n015_successMovesTillTheEndOfCompetition() throws Exception {
        getNode().waitHeight(ctx()._blocksOnGameStart.getValue());

        int round = 0;
        int competitionEnd = getRoundCompetitionEndHeight(round);
        int moveSeq = 0;
        int currHeight;
        boolean oneMoveInLastBlockDone = false;
        long pauseBetweenMoves = ConfigITest.NODE_API_AVG_BLOCK_DELAY / 4;

        while (((currHeight = getNode().getHeight()) < competitionEnd)
                        && !oneMoveInLastBlockDone) {
            PrivateKeyAccount acc = nextPlayer(moveSeq++);
            moveAndAssertMovedHeight(acc, round);
            oneMoveInLastBlockDone = currHeight == competitionEnd - 1;
            getLogger().info("{} sec pause between moves: currHeight={} competitionEnd={}", pauseBetweenMoves, currHeight, competitionEnd);
            Thread.sleep(pauseBetweenMoves);
        }
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Last competition block has been reached. DISALLOW anyone to invoke @move.")
    public void n020_failToMoveOnCompetitionEndBlock() throws Exception {
        getNode().waitHeight(getRoundCompetitionEndHeight(0));
        invokeMove(getLooserAcc());
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Pause Stage. DISALLOW anyone to invoke @move.")
    public void n025_failToMoveAtPauseStage() throws Exception {
        getNode().waitHeight(getRoundCompetitionEndHeight(0) + 1);
        invokeMove(getLooserAcc());
    }

    @Test(expected = IOException.class)
    @Description("Round 0. Pause Stage. DISALLOW anyone to invoke @move at round 0 second-to-last block (Round 1 starting block minus 1)")
    public void n030_failToMoveBefore1BlockDiffFromR1Start() throws Exception {
        getNode().waitHeight(getRoundStartingHeight(1) - 1);
        invokeMove(getLooserAcc());
    }

    @Test
    @Description("Round 1. Competition Stage. Round 1 starting height. ALLOW Alice to invoke @move at exact round 1 starting height.")
    public void n035_successMoveAtExactR1StartHeight() throws Exception {
        getNode().waitHeight(getRoundStartingHeight(1));
        invokeMove(ctx().aliceAcc);
    }

    @Test
    @Description("Round 1. Competition Stage. ALLOW Round 0 winner to claim win amount")
    public void n040_successWithdrawR0() throws Exception {
        withdrawAndAssertBalance(getBenzAcc(), ctx().winnersByRound.get(0), 0);
    }

    @Test(expected = IOException.class)
    @Description("Round 1. Competition Stage. DISALLOW to invoke @move with amount less than min in attached payment")
    public void n045_failToMoveR1WithAmtLessThanMin() throws Exception {
        invokeMove(getLooserAcc(), ctx()._pmtStep.getValue() - 1, ctx()._mrtAssetId.getValue());
    }

    @Test
    @Description("Round 1. Competition Stage. ALLOW to invoke @move with amount greater than min in attached payment")
    public void n050_successToMoveR1WithAmtGreaterThanMin() throws Exception {
        invokeMove(getLooserAcc(), ctx()._pmtStep.getValue() + 1, ctx()._mrtAssetId.getValue());
    }

    @Test
    @Description("Round 1. Competition Stage. ALLOW to invoke @move with amount equals to min in attached payment")
    public void n055_successMoveR1WithCorrectAmt() throws Exception {
        invokeMove(getLooserAcc(), ctx()._pmtStep.getValue(), ctx()._mrtAssetId.getValue());
    }

    @Test(expected = IOException.class)
    @Description("Round 1. Competition Stage. DISALLOW to invoke @move with amount equals to min BUT with invalid asset in attached payment")
    public void n060_failToMoveR1WithInvalidAsset() throws Exception {
        invokeMove(getLooserAcc(), ctx()._pmtStep.getValue(), null);
    }

    @Test
    @Description("Round 1. Competition Stage. ALLOW to invoke @move with amount equals to min and valid asset in attached payment")
    public void n065_successMoveR1WithValidAsset() throws Exception {
        invokeMove(getLooserAcc(), ctx()._pmtStep.getValue(), ctx()._mrtAssetId.getValue());
    }

    @Test(expected = IOException.class)
    @Description("Round 1. Win height has been reached. Win height less than competition end height. " +
            "DISALLOW to invoke valid @move")
    public void n070_failToMoveAfterR1Countdown() throws Exception {
        getNode().waitHeight(ctx().sharedState.currWinHeight);
        invokeMove(getLooserAcc());
    }

    @Test(expected = IOException.class)
    @Description("Round 2 starting height has been reached. Max rounds in smart script is 2. DISALLOW any to invoke @move.")
    public void n075_failToMoveInR2BecauseMaxRoundsEq2() throws Exception {
        getNode().waitHeight(getRoundStartingHeight(2));
        invokeMove(ctx().aliceAcc);
    }

    private PrivateKeyAccount nextPlayer(int moveNum) {
        int q = moveNum % 3;
        if (q == 0) {
            return ctx().aliceAcc;
        } else if (q == 1) {
            return ctx().bobAcc;
        } else {
            return ctx().caiAcc;
        }
    }

    private void moveAndAssertMovedHeight(PrivateKeyAccount acc, int round) throws IOException, InterruptedException {
        String moveTxId = invokeMove(acc);
        int txHeight = getNode().getTransaction(moveTxId).getHeight();
        if (txHeight <= getHeightWhenFullStepIsUsed(round)) {
            Assert.assertEquals(txHeight + ctx()._heightStep.getValue(), ctx().sharedState.currWinHeight);
        } else {
            Assert.assertEquals(getRoundCompetitionEndHeight(round), ctx().sharedState.currWinHeight);
        }
    }
}